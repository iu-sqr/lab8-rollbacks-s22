from typing import Tuple, Optional, List

import psycopg2

RESPONSE_STATUS = Tuple[Optional[int], Optional[int], List[Tuple[str, int]]]

price_request = "select price from shop where product = %(product)s"
buy_decrease_balance = (
    "update player "
    f"set balance = balance - ({price_request}) * %(amount)s "
    "where username = %(username)s "
    "returning balance"
)
buy_decrease_stock = "update shop set in_stock = in_stock - %(amount)s where product = %(product)s returning in_stock"
buy_increase_inventory = (
    "insert into inventory (username, product, amount) "
    "values (%(username)s, %(product)s, %(amount)s) "
    "on conflict (username, product) do update "
    "set amount = excluded.amount + inventory.amount "
    "returning amount"
)

get_balance = "select balance from player where username=%(username)s"
get_stock = "select in_stock from shop where product = %(product)s"
get_inventory = "select product, amount from inventory where username=%(username)s"


def get_conn():
    return psycopg2.connect(
        dbname="lab",
        host="localhost",
        port=5432,
        password="mysecretpassword",
        user="postgres",
    )


def execute_buy(
    conn,
    username: str,
    product: str,
    amount: int,
) -> str:
    with conn:
        with conn.cursor() as cur:
            obj = {
                "product": product,
                "username": username,
                "amount": amount,
            }

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    return "Wrong username"
            except psycopg2.errors.CheckViolation:
                return "Bad balance"

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    return "Wrong product or out of stock"
            except psycopg2.errors.CheckViolation:
                return "Product is out of stock"

            try:
                cur.execute(buy_increase_inventory, obj)
            except psycopg2.errors.RaiseException:
                return "Reached inventory limit"

            conn.commit()
            return "Successful purchase"


def get_status(conn, username: str, product: str) -> RESPONSE_STATUS:
    with conn:
        with conn.cursor() as cur:
            balance = None
            try:
                cur.execute(get_balance, {"username": username})
                if cur.rowcount == 1:
                    (balance,) = cur.fetchone()
            except psycopg2.errors.CheckViolation:
                pass

            in_stock = None
            try:
                cur.execute(get_stock, {"product": product})
                if cur.rowcount == 1:
                    (in_stock,) = cur.fetchone()
            except psycopg2.errors.CheckViolation:
                pass

            inventory = []
            try:
                cur.execute(get_inventory, {"username": username})
                inventory = list(cur.fetchall())
            except psycopg2.errors.CheckViolation:
                pass

            return balance, in_stock, inventory
