import typer

import db

app = typer.Typer()


def validate_positive_amount(amount: int) -> int:
    if amount <= 0:
        raise typer.BadParameter("Amount must be positive")
    return amount


@app.command()
def buy_product(
    username: str,
    product: str,
    amount: int = typer.Argument(..., callback=validate_positive_amount),
):
    conn = db.get_conn()
    purchase_result = db.execute_buy(conn, username, product, amount)
    balance, in_stock, inventory = db.get_status(conn, username, product)

    typer.echo(purchase_result)
    typer.echo()
    typer.echo(f"{username} balance: {balance}")
    typer.echo(f"{product} stock: {in_stock}")
    typer.echo(
        f"{username} inventory:\n  * "
        + "\n  * ".join(
            f"{inventory_product}: {inventory_amount}"
            for (inventory_product, inventory_amount) in inventory
        )
    )


if __name__ == "__main__":
    app()
