insert into player (username, balance)
values ('Alice', 100);
insert into player (username, balance)
values ('Bob', 200);

insert into shop (product, in_stock, price)
values ('marshmallow', 10, 10);
insert into shop (product, in_stock, price)
values ('potato', 150, 1);
