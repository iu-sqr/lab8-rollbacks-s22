create table player
(
    username TEXT primary key,
    balance  INT check (balance >= 0)
);
create table shop
(
    product  TEXT primary key,
    in_stock INT check (in_stock >= 0),
    price    INT check (price >= 0)
);
create table inventory
(
    username TEXT references player,
    product  TEXT references shop,
    amount   INT check ( amount >= 0 ),

    primary key (username, product)
);


create or replace function validate_inventory_size()
    returns trigger as
$BODY$
declare
    inventory_filled_amount  int;
    inventory_limit constant int := 100;
begin
    select sum(amount) + new.amount
    into inventory_filled_amount
    from inventory
    where username = new.username;

    if inventory_filled_amount > inventory_limit then
        raise exception 'Cannot have more than % elements, but wanted %', inventory_limit, inventory_filled_amount;
    end if;

    return new;
end;
$BODY$
    language plpgsql;

create trigger validate_inventory_time
    before insert
    on inventory
    for each row
execute procedure validate_inventory_size();